import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import numpy as np
import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd


# # Use a service account
# cred = credentials.Certificate('./cred3.json')
# firebase_admin.initialize_app(cred)

# db = firestore.client()

# users_refc = db.collection(u'Clientes')
# docsc = users_refc.stream()

# users_refA = db.collection(u'Asistentes')
# docsA = users_refA.stream()

# users_refD = db.collection(u'DiscapacidadClientes')
# docsD = users_refD.stream()

# users_refP = db.collection(u'PeticionesDentroDeCasa')
# docsP = users_refP.stream()

# users_refPA = db.collection(u'PeticionesFueraDeCasa')
# docsPA = users_refPA.stream()


# clientes = []
# clientesid = []
# asistentes = []
# asistentesid = []
# discapacidad = []
# discapacidadid =[]
# peticionesA = []
# peticionesAid = []
# peticionesF = []
# peticionesFid = []

# for doc in docsc:
	# print('entro')
	# clientes.append(doc.to_dict())
	# clientesid.append(doc.id)
	
# for doc in docsA:
	# print('entro')
	# asistentes.append(doc.to_dict())
	# asistentesid.append(doc.id)

# for doc in docsD:
	# print('entro')
	# discapacidad.append(doc.to_dict())
	# discapacidadid.append(doc.id)
	
# for doc in docsP:
	# print('entro')
	# peticionesA.append(doc.to_dict())
	# peticionesAid.append(doc.id)

# for doc in docsPA:
	# print('entro')
	# peticionesF.append(doc.to_dict())
	# peticionesFid.append(doc.id)
	
#print(clientes[0]['email'])
my_data = np.genfromtxt('data2.csv', delimiter=',',names=True,dtype=None)
my_data2 = np.genfromtxt('data3.csv', delimiter =',', names=True, dtype= None)


my_data3 = pd.read_csv('data4.csv',sep =',')
my_data3 = [[0,1],[1,0.33],[2,0.55],[3,0.25],[4,0],[5,0]]
my_data2 = []
weeks = []
nameweeks = []
days =[]
daysname =[]
# for i in range(0,len(my_data2)):
	# weeks.append(my_data2[i][1])
	# nameweeks.append(my_data2[0])
weeks = [0,0,0,0,3,14]
nameweeks =['1 sept. - 7 sept.','1 sept. - 7 sept.','1 sept. - 7 sept.','1 sept. - 7 sept.','1 sept. - 7 sept.','1 sept. - 7 sept.']
print(type(my_data3[0]))
print(my_data3[0])
for i in range(0,len(my_data2)):
	days.append(my_data3[i][1])
	daysname.append(my_data3[i][0])
days = [1,0.33,0.55,0.25,0,0]
porcentaje = []
labelporcentaje = []
numHora = []
for i in range(0, len(my_data)):
	porcentaje.append(my_data[i][1])
	numHora.append(my_data[i][2])
	labelporcentaje.append(my_data[i][0].decode('UTF-8'))
print(type(labelporcentaje[0]))
numpA = 15
numpF = 5
print(days)
numD = 0.1
numA = 0.2
numC = 0.3
numF =0.3
numV = 0.1

# for i in range(0, len(discapacidad)):
	# numD = numD + discapacidad[i]['auditiva']+ discapacidad[i]['cognitiva'] + discapacidad[i]['fisica'] +  discapacidad[i]['visual']
	# numaA = numA + discapacidad[i]['auditiva']
	# numaC = numC + discapacidad[i]['cognitiva'] 
	# numaF = numF + discapacidad[i]['fisica']
	# numaV = numV + discapacidad[i]['visual']
numm = [numA,numC,numF,numV]
numlable = ['auditiva','cognitiva','fisica','visual']
app = dash.Dash()
app.css.append_css({'external_url': 'https://codepen.io/amyoshino/pen/jzXypZ.css'})


app.layout = html.Div(
    html.Div([
        html.Div(
            [
                html.H1(children='Analytics',
                        className='nine columns'),
                html.Div(children='''
                        Information gotten through the app.
                        ''',
                        className='nine columns'
                ),
				html.Div(children='''
                        En total hay numero de personas con discapacidades: 6.
                        ''',
                        className='nine columns'
                ),
				html.Div(children='''
                        Hay un total de 14 usuarios.
                        ''',
                        className='nine columns'
                ),
								html.Div(children='''
                        Hay un total de 10 asistentes.
                        ''',
                        className='nine columns'
                ),
						html.Div(children='''
                        Paises: Colombia.
                        ''',
                        className='nine columns'
                ),
            ], className="row"
        ),
        html.Div(
            [
            html.Div([
                dcc.Graph(
                    id='example-graph',
                    figure={
                        'data': [
                            {'x': [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], 'y': numHora, 'type': 'bar', 'name': labelporcentaje},

						


                        ],
                        'layout': {
                            'title': 'Average time use in each window',
                            'xaxis' : dict(
                                title='x Axis',
                                titlefont=dict(
                                family='Courier New, monospace',
                                size=20,
                                color='#7f7f7f'
                            )),
                            'yaxis' : dict(
                                title='y Axis',
                                titlefont=dict(
                                family='Helvetica, monospace',
                                size=20,
                                color='#7f7f7f'
                            ))
                        }
                    }
                )
                ], className= 'six columns'
                ),

                html.Div([
                dcc.Graph(
                    id='example-graph-2',
                    figure={
                        'data': [
                            {
							'labels': labelporcentaje,
							'values': porcentaje,
							'type': 'pie', },
                        ],
                        'layout': {
                            'title': 'Porcentage by window'
                        }
                    }
                )
                ], className= 'six columns'
                )
            ], className="row"
	
        ),
        html.Div(
            [
            html.Div([
                dcc.Graph(
                    id='graph3',
                    figure={
                        'data': [
                            {'x': [1,2,3,4,5,6], 'y': weeks, 'type': 'linea', 'name': nameweeks},

						


                        ],
                        'layout': {
                            'title': 'Time by weeks',
                            'xaxis' : dict(
                                title='x Axis',
                                titlefont=dict(
                                family='Courier New, monospace',
                                size=20,
                                color='#7f7f7f'
                            )),
                            'yaxis' : dict(
                                title='y Axis',
                                titlefont=dict(
                                family='Helvetica, monospace',
                                size=20,
                                color='#7f7f7f'
                            ))
                        }
                    }
                )
                ], className= 'six columns'
                ),

                html.Div([
                dcc.Graph(
                    id='graph4',
                    figure={
                        'data': [
						{'x': [1,2,3,4,5], 'y': days, 'type': 'linea'},
                        ],
                        'layout': {
                            'title': 'Time ask outside and times inside'
                        }
                    }
                )
                ], className= 'six columns'
                )
            ], className="row"
	
        ),	
        html.Div(
            [
                html.Div([
                dcc.Graph(
                    id='graph5',
                    figure={
                        'data': [
                            {
							'labels': numlable,
							'values': numm,
							'type': 'pie', },
                        ],
                        'layout': {
                            'title': 'Discapacidades con promedio'
                        }
                    }
                )
                ], className= 'six columns'
                ),

                html.Div([
                dcc.Graph(
                    id='graph6',
                    figure={
                        'data': [
                            {
							'labels': ['pedidos Afuera', 'Pedidos Adentro'],
							'values': [numpA,numpF],
							'type': 'pie', },
                        ],
                        'layout': {
                            'title': 'Time ask outside and times inside'
                        }
                    }
                )
                ], className= 'six columns'
                )
            ], className="row"
	
        ),			
    ], className='ten columns offset-by-one')
)

if __name__ == '__main__':
    app.run_server(debug=True)